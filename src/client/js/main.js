import brainfuck from './brainfuck.js';
import loadDOM from './loadDOM.js';

window.onload = () => 
{
    const DOM = loadDOM();
    DOM.runBtn.addEventListener('click', async () =>
    {
        for (const tool of DOM.tools) tool.disabled = true;
        try
        {
            DOM.outp.innerText = await brainfuck(DOM);
        }
        catch (e)
        {
            DOM.outp.innerText = e.message;
        }
        for (const tool of DOM.tools) tool.disabled = false;
    });

    DOM.memClearBtn.addEventListener('click', () =>
    {
        DOM.memory.innerHTML = '';
    });
};
