import { sleep } from './tools.js';
const bfRegex = /(,|\.|\[|\]|>|<|\+|-)/g;
const dataSize = 30000;
const stackOverflow = 1e9; // Execute maximum 1 million commands

export default async function brainfuck (DOM)
{
  const clearedCode = DOM.editor.getValue().match(bfRegex).join('')
  const data = [0];
  let pointer = 0;
  const changePointer = val =>
    {
      pointer += val;
      // Wrap pointer around
      if (pointer < 0) pointer = dataSize-1;
      else if (pointer >= dataSize) pointer = 0;
      if (typeof data[pointer] != 'number') data[pointer] = 0;
    };
  const changeValue = val =>
    {
      data[pointer] += val;
      if (data[pointer] < 0) throw new Error('Value of cell below zero!');
    };
  const numToASCII = nr => String.fromCharCode(nr);
  const ASCIItoNum = char => char?.charCodeAt(0);
  const memField = DOM.memory;
  const updateMemory = () =>
    {
      memField.innerHTML = '';
      for (const i in data)
      {
        if (data[i] == undefined) continue;
        const block = document.createElement('div');
        block.classList.add('memBlock');
        const indexDisplay = document.createElement('span');
        indexDisplay.innerText = i;
        indexDisplay.classList.add('indexDisplay');
        const valueDisplay = document.createElement('span');
        valueDisplay.classList.add('valueDisplay');
        valueDisplay.innerText = data[i];
        // TODO: display ASCII
        block.append(indexDisplay, valueDisplay);
        memField.appendChild(block);
      }
    }

  const PAUSE = 500;
  const codeBuffer = clearedCode.split('');
  const input = DOM.inputField.value.split('');
  let inputPointer = 0;
  let output = '';
  let callCount = 0;
  for (let i = 0; i < codeBuffer.length; i++)
  {
    if (i % PAUSE == 0) await sleep(50); // Pause every x commands so that the DOM can update and tab doesnt crash
    if (++callCount > stackOverflow)
      {
        updateMemory();
        throw new Error('Infinite loop detected. Stopping programm.');
      }
    const command = codeBuffer[i];
    switch (command)
    {
      case '.':
        output += numToASCII(data[pointer]);
        break;
      case ',':
        data[pointer] = ASCIItoNum(input[inputPointer++]);
        break;
      case '+':
        changeValue(1);
        break;
      case '-':
        changeValue(-1);
        break;
      case '[':
        if (data[pointer] == 0)
          {
            let bracketCounter = 1;
            while (bracketCounter > 0)
              {
                while (codeBuffer[i] != ']')
                {
                  if (codeBuffer[i] == '[') bracketCounter++;
                  i++;
                    console.log(i);
                }
                bracketCounter--;
              }
          }
        break;
      case ']':
        if (data[pointer] > 0)
          {
            let bracketCounter = 1;
            while (bracketCounter > 0)
              {
                while (codeBuffer[i] != '[')
                {
                  if (codeBuffer[i] == ']') bracketCounter++;
                  i--;
                }
                bracketCounter--;
              }
          }
        break;
      case '>':
        changePointer(1);
        break;
      case '<':
        changePointer(-1);
        break;
    }
  }

  updateMemory();
  return output;
};
