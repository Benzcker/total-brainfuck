export function setupEditor ()
{
    const codeWindow = document.querySelector('#codemirror-textarea');
    const editor = CodeMirror.fromTextArea(codeWindow,
        {
            mode: 'brainfuck',
            lineNumbers: true,
            lineWrapping: false,
            lint: true,
            foldGutter: true,
            gutters: ["CodeMirror-lint-markers","CodeMirror-linenumbers", "CodeMirror-foldgutter"],
            tabSize: 2,
            indentUnit: 2,
            matchBrackets: true,
            theme: 'monokai',
            value: 'Read all the inputs',
            autofocus: true,
        });

    editor.setSize(null, '100%');

    setTimeout(() =>
    {
        codeWindow.value = `
Read everything
,[>,]

Output everything in reverse
<[.<]
`;
    }, 300);
  return editor;
};
