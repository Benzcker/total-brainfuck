import { setupEditor } from './editor.js';

const loadDOM = () =>
({
    editor: setupEditor(),
    tools: document.querySelectorAll('.tool'),
    runBtn: document.querySelector('#run'),
    memClearBtn: document.querySelector('#memClear'),
    inputField: document.querySelector('#input input'),
    outp: document.querySelector('#output'),
    memory: document.querySelector('#memory'),
});
export default loadDOM;
