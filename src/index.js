const express = require('express');
const app = express();

app.use('/', express.static(__dirname + '/client'))

const PORT = 5000;
app.listen(PORT, () => console.log(`Brainfuck Server listening on port ${PORT}`));
